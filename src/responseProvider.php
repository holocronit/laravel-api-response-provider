<?php

namespace Holocronit\LaravelApiResponseProvider;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Crypt;


class responseProvider extends ServiceProvider
{

    public $result              = TRUE;
    public $error               = FALSE;
    public $token               = FALSE;
    public $token_data          = FALSE;
    public $localizationError   = FALSE;

    private $crypt              = FALSE;

    public $data                = array();
    private $errorMsg           = array();
    private $rightMsg           = array();


    CONST TOKEN_KEY             = 'access_token';
    CONST ERROR_KEY             = 'error';
    CONST RESULT_KEY            = 'result';
    CONST DEFAULT_ERROR_MSG     = 'Undefined error';

    CONST EXPIRES_TIME          = '1970-01-01 00:00:00';
    CONST DEFAULT_LOCALE        = 'en';


    public function __construct()
    {
        $this->localizationError    = self::DEFAULT_LOCALE;
        if ( file_exists( config_path() . '/errors_list.json' ) ) {
            $errorJsonMsg           = file_get_contents( config_path() . '/errors_list.json' );
            $errorJsonMsg           = json_decode( $errorJsonMsg );
            $this->errorMsg         = $errorJsonMsg->{$this->localizationError};
        }
    }


    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        error_log('boot-response');
        $this->publishes([
            __DIR__ . '/errors_list.json' => config_path('errors_list.json'),
        ], 'errors-config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        error_log('register-response');
    }

    /**
     * Change the dafult locale
     *
     * @return void
     */

    public function loadErrorListForLocale ( $locale = 'en' )
    {
        $this->localizationError        = $locale;
        if ( file_exists( config_path() . '/errors_list.json' ) ) {
            $errorJsonMsg               = file_get_contents( config_path() . '/errors_list.json' );
            $errorJsonMsg               = json_decode( $errorJsonMsg );
            if ( isset($errorJsonMsg->{$this->localizationError}) ) {
                $this->errorMsg             = $errorJsonMsg->{$this->localizationError};
            } else {
                $this->localizationError    = self::DEFAULT_LOCALE;
                $this->errorMsg             = $errorJsonMsg->{$this->localizationError};
            }
        }
    }


    public function addData( $data = FALSE , $key = FALSE )
    {
        // if( $data ){
            if($key){
                $this->data[ $key ] = $data;
            }else{
                if(is_array($data)){
                     $this->data = array_merge( $this->data , $data );
                }
            }
        // }
    }

    public function generate_token ( $user_id , $expire_time = self::EXPIRES_TIME  , $extra_data = array()) {

        if( empty( $user_id )  ) return '';

        $token_data                 = array();
        $token_data['id']           = intval($user_id);
        $token_data['expireDate']   = $expire_time;
        $token_data['rand']         = rand(1000000,9999999);
        $token_data                 = array_merge( $token_data , $extra_data );

        $token_string   = json_encode( $token_data );

        $token = Crypt::encrypt( '#*#*'.$token_string.'a1b2c3d4' );

        $this->token        = $token;
        $this->token_data   = $token_data;

        return $token;        


    }


    public function read_token( $token = FALSE ){

        if( !$token || empty($token)) return FALSE;

        if ( strlen($token) < 50 ) return FALSE;

        // Token Decryption
        $token_data = Crypt::decrypt(  $token  );
        
        // Read Token
        if ( $token_data ) $token_data = json_decode( str_replace(array('#*#*','a1b2c3d4'),'',$token_data) , TRUE );

        $this->token        = $token;
        $this->token_data   = $token_data;

        return $token_data;

    }




    public function addToken( $token = FALSE )
    {
        if( !empty($token) ){
            $this->token = trim($token);
        }
    }


    public function getResponseData()
    {
        
        $response = array();

        $response[ $this::RESULT_KEY ] = $this->result;
        if( $this->error ){
            $response[ $this::ERROR_KEY ] = $this->error;
        } 


        if( $this->token ){
            $response[ $this::TOKEN_KEY ] = $this->token;
        } 

        $response = array_merge( $response , $this->data );

        return $response;

    }





    public function inError( $errorCode = -1, $customError = false )
    {   

        $this->result = FALSE;

        if ( $customError ) {
            $errorDesc  = $customError;
        } else if ( isset($this->errorMsg->$errorCode) ) {
            $errorDesc  = $this->errorMsg->$errorCode;
        } else {
            $errorDesc  = self::DEFAULT_ERROR_MSG;
        }

        // $errorDesc .= ' - #'.str_pad( $errorCode , 6, 0 , STR_PAD_LEFT  );

        $this->error = [ 'code' => $errorCode , 'desc' => $errorDesc  ];
    }



}
